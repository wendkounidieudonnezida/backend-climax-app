package fr.agritex.burkina.testechnique.dieudonne.climaxClientState;

import fr.agritex.burkina.testechnique.dieudonne.climaxClientState.entities.Client;
import fr.agritex.burkina.testechnique.dieudonne.climaxClientState.repositories.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing
public class ClimaxClientStateApplication implements CommandLineRunner {

	@Autowired
	private ClientRepository clientRepository;

	public static void main(String[] args) {
		SpringApplication.run(ClimaxClientStateApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		//clientRepository.save(new Client((Long) null,"Dieudonne","ZIDA", 10,"Developpeur", 12345678));
	}
}
