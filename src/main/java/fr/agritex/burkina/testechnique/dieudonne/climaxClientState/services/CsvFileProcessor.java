package fr.agritex.burkina.testechnique.dieudonne.climaxClientState.services;


import fr.agritex.burkina.testechnique.dieudonne.climaxClientState.entities.Client;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Service
public class CsvFileProcessor  implements FileProcessorService{
    @Autowired
    private ClientService clientService;

    @Override
    public String getSupportedFileType() {
        return "csv";
    }

    @Override
    public void process(String filePath) {
        Path path = Paths.get(filePath);

        try (Reader reader = Files.newBufferedReader(path);
             CSVParser csvParser = new CSVParser(reader, CSVFormat.DEFAULT.withTrim())) {

            for (CSVRecord record : csvParser) {
                String nom = record.get(0);
                String prenom = record.get(1);
                int age = Integer.parseInt(record.get(2));
                String profession = record.get(3);

                // Supprimez l'espace et le point-virgule de la fin de la chaîne si présents
                String salaireStr = record.get(4).trim();
                salaireStr = salaireStr.replaceAll("[^\\d.]", "");
                int salaire = Integer.parseInt(salaireStr);

                 Client client = new Client(null,nom, prenom, age, profession, salaire);
                 clientService.save(client);
            }

        } catch (IOException e) {
            e.printStackTrace();

        }
    }
}
