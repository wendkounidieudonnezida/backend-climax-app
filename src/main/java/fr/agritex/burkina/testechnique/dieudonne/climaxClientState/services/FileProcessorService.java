package fr.agritex.burkina.testechnique.dieudonne.climaxClientState.services;


public interface FileProcessorService {

    String getSupportedFileType();
    void process(String filePath);

}
