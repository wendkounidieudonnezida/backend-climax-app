package fr.agritex.burkina.testechnique.dieudonne.climaxClientState.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement(name = "ClientsDTO")
@XmlAccessorType(XmlAccessType.NONE) // Ignore les champs par défaut
public class ClientsDTO {
    private List<ClientDTO> clients = new ArrayList<>();

    @XmlElement(name = "ClientDTO")
    public List<ClientDTO> getClients() {
        return clients;
    }
}

