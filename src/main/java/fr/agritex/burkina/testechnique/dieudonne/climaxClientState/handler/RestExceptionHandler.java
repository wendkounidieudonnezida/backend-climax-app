package fr.agritex.burkina.testechnique.dieudonne.climaxClientState.handler;

import fr.agritex.burkina.testechnique.dieudonne.climaxClientState.exceptions.EntityNotFoundException;
import fr.agritex.burkina.testechnique.dieudonne.climaxClientState.exceptions.InvalidEntityException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@RestControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {
    @ExceptionHandler(EntityNotFoundException.class)
    public ResponseEntity<ErrorDTO> handlerException(EntityNotFoundException entityNotFoundException, WebRequest webRequest){

        final HttpStatus notFound = HttpStatus.NOT_FOUND;
        final ErrorDTO errorDTO = new ErrorDTO();
        errorDTO.setHttpCode(notFound.value());
        errorDTO.setMessage(entityNotFoundException.getMessage());

        return new ResponseEntity<>(errorDTO,notFound);
    }

    @ExceptionHandler(InvalidEntityException.class)
    public ResponseEntity<ErrorDTO> handlerException(InvalidEntityException invalidEntityException, WebRequest webRequest){

        final HttpStatus notFound = HttpStatus.BAD_REQUEST;
        final ErrorDTO errorDTO = new ErrorDTO();
        errorDTO.setHttpCode(notFound.value());
        errorDTO.setMessage(invalidEntityException.getMessage());
        errorDTO.setErroList(invalidEntityException.getErrList());

        return new ResponseEntity<>(errorDTO,notFound);
    }
}
