package fr.agritex.burkina.testechnique.dieudonne.climaxClientState.exceptions;

import lombok.Data;

import java.util.List;

@Data
public class InvalidEntityException extends RuntimeException{

    private List<String> errList;

    InvalidEntityException(String message){
        super(message);
    }

    InvalidEntityException(String message,Throwable cause){
        super(message,cause);
    }

    public InvalidEntityException(String message, List<String> errList){
        super(message);
        this.errList = errList;
    }
}
