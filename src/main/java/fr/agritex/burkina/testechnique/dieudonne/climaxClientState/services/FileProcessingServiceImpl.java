package fr.agritex.burkina.testechnique.dieudonne.climaxClientState.services;

import fr.agritex.burkina.testechnique.dieudonne.climaxClientState.utils.FileUtils;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Service
public class FileProcessingServiceImpl implements FileProcessingService{

    private final Map<String, FileProcessorService> processors;

    // Injectez les processors
    public FileProcessingServiceImpl(List<FileProcessorService> processorList) {
        processors = new HashMap<>();
        processorList.forEach(processor ->
                processors.put(processor.getSupportedFileType(), processor));
    }

    @Override
    public void processFile(String filePath) {
        String fileType = FileUtils.getFileExtension(filePath);

        FileProcessorService processor = processors.get(fileType);
        if (processor == null) {
            throw new IllegalArgumentException("Unsupported file type: " + fileType);
        }

        processor.process(filePath);
    }

}
