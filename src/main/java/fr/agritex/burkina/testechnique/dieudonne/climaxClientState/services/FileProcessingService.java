package fr.agritex.burkina.testechnique.dieudonne.climaxClientState.services;


public interface FileProcessingService {

    void processFile (String filePath);
}
