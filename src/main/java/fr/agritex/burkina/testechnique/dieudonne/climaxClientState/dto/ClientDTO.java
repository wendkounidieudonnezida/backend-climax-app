package fr.agritex.burkina.testechnique.dieudonne.climaxClientState.dto;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@Data @NoArgsConstructor @AllArgsConstructor

public class ClientDTO {

    private Long id;
    private String nom;
    private String prenom;
    private Integer age;
    private String profession;
    private Integer salaire;
}
