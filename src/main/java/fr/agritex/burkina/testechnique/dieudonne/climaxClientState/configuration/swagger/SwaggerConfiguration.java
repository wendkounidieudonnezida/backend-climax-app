package fr.agritex.burkina.testechnique.dieudonne.climaxClientState.configuration.swagger;

import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import static fr.agritex.burkina.testechnique.dieudonne.climaxClientState.utils.Constant.APP_ROOT;

@Configuration
@EnableSwagger2
public class SwaggerConfiguration {
    public Docket api (){
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(
                        new ApiInfoBuilder()
                                .description("Gestion des client de climax")
                                .title("climaxClientState")
                                .build()
                )
                .groupName("API V1")
                .select()
                .apis(RequestHandlerSelectors.basePackage("fr.agritex.burkina.testechnique.dieudonne.climaxClientState.api"))
                .paths(PathSelectors.ant(APP_ROOT+ "/**"))
                .build();
    }
}

