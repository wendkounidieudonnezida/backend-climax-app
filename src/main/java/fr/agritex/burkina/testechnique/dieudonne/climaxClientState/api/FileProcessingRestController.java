package fr.agritex.burkina.testechnique.dieudonne.climaxClientState.api;


import io.swagger.annotations.Api;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import static fr.agritex.burkina.testechnique.dieudonne.climaxClientState.utils.Constant.APP_ROOT;

@Api(APP_ROOT+"/processing/file")
@RequestMapping(APP_ROOT+"/processing/file")
public interface FileProcessingRestController {

    @PostMapping("/upload")
    ResponseEntity<String>uploadFile (@RequestParam("file") MultipartFile file);
}
