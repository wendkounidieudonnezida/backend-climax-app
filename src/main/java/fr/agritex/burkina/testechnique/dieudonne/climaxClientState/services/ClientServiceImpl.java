package fr.agritex.burkina.testechnique.dieudonne.climaxClientState.services;

import fr.agritex.burkina.testechnique.dieudonne.climaxClientState.dto.ClientDTO;
import fr.agritex.burkina.testechnique.dieudonne.climaxClientState.entities.Client;
import fr.agritex.burkina.testechnique.dieudonne.climaxClientState.mappers.ClientMapper;
import fr.agritex.burkina.testechnique.dieudonne.climaxClientState.repositories.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

@Service
public class ClientServiceImpl implements ClientService{
    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    private ClientMapper clientMapper;

    @Override
    public ClientDTO save(Client client) {
        return clientMapper.EntityToDTO(clientRepository.save(client));
    }

    @Override
    public ClientDTO getClientById(Long id) {

        return clientMapper.EntityToDTO(requireOne(id)) ;//verifie l'existence de l'entite via la methode requiOne

    }

    @Override
    public Page<Client> getAllClient(Pageable pageable) {

        Page pageClient = clientRepository.findAll(pageable);
        return pageClient;
    }

    @Override
    public Page<Client> findByKeyboard(String keyboard, Pageable pageable) {
        return clientRepository.findByKeyboard(keyboard,pageable);
    }

    @Override
    public void deleteClient(Long id) {
        clientRepository.delete(requireOne(id));  //verifie l'existence de l'entite via la methode requiOne
    }

    @Override
    public Map<String, Double> calculerSalaireMoyenParProfession() {

        return clientRepository.findAll().stream()
                .filter(personne -> personne.getProfession() != null && personne.getSalaire() != null)
                .collect(Collectors.groupingBy(
                        Client::getProfession,
                        Collectors.averagingDouble(Client::getSalaire)
                ));
    }



    //Verifier l'existence d'une entité dans la db//
    private Client requireOne(Long id) {
        return clientRepository.findById(id)
                .orElseThrow(() -> new NoSuchElementException("Client non trouver: " + id));
    }
}
