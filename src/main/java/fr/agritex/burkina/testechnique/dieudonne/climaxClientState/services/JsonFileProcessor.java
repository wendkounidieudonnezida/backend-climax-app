package fr.agritex.burkina.testechnique.dieudonne.climaxClientState.services;

import com.fasterxml.jackson.core.exc.StreamReadException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DatabindException;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.agritex.burkina.testechnique.dieudonne.climaxClientState.dto.ClientDTO;
import fr.agritex.burkina.testechnique.dieudonne.climaxClientState.entities.Client;
import fr.agritex.burkina.testechnique.dieudonne.climaxClientState.mappers.ClientMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.List;

@Service
public class JsonFileProcessor implements FileProcessorService {
    @Autowired
    private ClientService clientService;
    @Autowired
    private ClientMapper clientMapper;

    @Override
    public String getSupportedFileType() {
        return "json";
    }

    @Override
    public void process(String filePath) {
        ObjectMapper objectMapper = new ObjectMapper();

        try {
            List<ClientDTO> dataObjects = objectMapper.readValue(Paths.get(filePath).toFile(),
                    new TypeReference<>() {
                    });

            // Traitement des objets
            for (ClientDTO clientDTO : dataObjects) {
                clientService.save(clientMapper.DTOtoEntity(clientDTO));
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
