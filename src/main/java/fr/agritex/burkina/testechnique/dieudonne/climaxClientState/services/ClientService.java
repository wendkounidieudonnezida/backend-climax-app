package fr.agritex.burkina.testechnique.dieudonne.climaxClientState.services;



import fr.agritex.burkina.testechnique.dieudonne.climaxClientState.dto.ClientDTO;
import fr.agritex.burkina.testechnique.dieudonne.climaxClientState.entities.Client;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Map;

public interface ClientService {

    ClientDTO save(Client client);

    ClientDTO getClientById(Long id);

    Page<Client> getAllClient(Pageable pageable);

    Page<Client> findByKeyboard(String keyboard, Pageable pageable);

    void deleteClient(Long id);

    Map<String, Double> calculerSalaireMoyenParProfession();

}

