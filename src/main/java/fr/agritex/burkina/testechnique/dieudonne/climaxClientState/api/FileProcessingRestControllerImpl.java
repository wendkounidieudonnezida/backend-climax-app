package fr.agritex.burkina.testechnique.dieudonne.climaxClientState.api;

import fr.agritex.burkina.testechnique.dieudonne.climaxClientState.services.FileProcessingService;
import fr.agritex.burkina.testechnique.dieudonne.climaxClientState.utils.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.nio.file.Path;

@RestController
public class FileProcessingRestControllerImpl implements FileProcessingRestController{
    @Autowired
    private FileProcessingService fileProcessingService;


    @Override
    public ResponseEntity<String> uploadFile(MultipartFile file) {

        try {
            String filename = file.getOriginalFilename();
            Path tempFile = FileUtils.saveFile(file);
            fileProcessingService.processFile(tempFile.toString());

            return ResponseEntity.ok("Fichier uploadé et traité avec succès: " + filename);

        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body("Erreur lors du traitement du fichier: " + e.getMessage());
        }
    }
}
