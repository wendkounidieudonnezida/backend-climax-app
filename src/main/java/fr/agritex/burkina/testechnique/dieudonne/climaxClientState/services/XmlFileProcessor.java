package fr.agritex.burkina.testechnique.dieudonne.climaxClientState.services;

import fr.agritex.burkina.testechnique.dieudonne.climaxClientState.dto.ClientDTO;
import fr.agritex.burkina.testechnique.dieudonne.climaxClientState.dto.ClientsDTO;
import fr.agritex.burkina.testechnique.dieudonne.climaxClientState.entities.Client;
import fr.agritex.burkina.testechnique.dieudonne.climaxClientState.mappers.ClientMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.nio.file.Paths;

@Service
public class XmlFileProcessor implements FileProcessorService {
    @Autowired
    private ClientService clientService;
    @Autowired
    private ClientMapper clientMapper;

    @Override
    public String getSupportedFileType() {
        return "xml";
    }

    @Override
    public void process(String filePath) {
        try {
            File file = Paths.get(filePath).toFile();
            JAXBContext jaxbContext = JAXBContext.newInstance(ClientsDTO.class);
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();

            ClientsDTO clientsDTO = (ClientsDTO) jaxbUnmarshaller.unmarshal(file);
            for (ClientDTO clientDTO : clientsDTO.getClients()) {
                Client client = clientMapper.DTOtoEntity(clientDTO);
                clientService.save(client);
            }

        } catch (JAXBException e) {
            e.printStackTrace();

        }
    }

}
