package fr.agritex.burkina.testechnique.dieudonne.climaxClientState.mappers;

import fr.agritex.burkina.testechnique.dieudonne.climaxClientState.dto.ClientDTO;
import fr.agritex.burkina.testechnique.dieudonne.climaxClientState.entities.Client;
import org.mapstruct.Mapper;

import javax.persistence.MappedSuperclass;
import java.util.List;

@Mapper(componentModel = "spring")
public interface ClientMapper {

    ClientDTO EntityToDTO (Client client) ;

    Client DTOtoEntity(ClientDTO clientDTO) ;

    List<ClientDTO> EntitiesToDTO(List<Client> clientList);

    List<Client> DTOtoEntities(List<ClientDTO> clientDTOList) ;
}
