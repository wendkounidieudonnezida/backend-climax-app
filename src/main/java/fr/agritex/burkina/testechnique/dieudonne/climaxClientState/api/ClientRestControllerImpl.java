package fr.agritex.burkina.testechnique.dieudonne.climaxClientState.api;

import fr.agritex.burkina.testechnique.dieudonne.climaxClientState.dto.ClientDTO;
import fr.agritex.burkina.testechnique.dieudonne.climaxClientState.entities.Client;
import fr.agritex.burkina.testechnique.dieudonne.climaxClientState.mappers.ClientMapper;
import fr.agritex.burkina.testechnique.dieudonne.climaxClientState.services.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class ClientRestControllerImpl implements ClientRestController{

    @Autowired
    private ClientService clientService;
    @Autowired
    private ClientMapper clientMapper;

    @Override
    public ClientDTO save(ClientDTO client) {
        return clientService.save(clientMapper.DTOtoEntity(client));
    }

    @Override
    public ClientDTO getClientById(Long id) {
        return clientService.getClientById(id);
    }

    @Override
    public ResponseEntity<Map<String, Object>> getAllAndfindByKeyboard(int page, int size, String search) {

        try {
            List<ClientDTO> clientList ;
            Pageable paging = PageRequest.of(page, size);
            Page<Client> clientPage;
            if (search == null){
                clientPage = clientService.getAllClient(paging);
            } else {
                clientPage = clientService.findByKeyboard(search.toLowerCase(),paging);
            }

            clientList = clientMapper.EntitiesToDTO(clientPage.getContent()) ;

            Map<String, Object> response = new HashMap<>();
            response.put("clientList", clientList);
            response.put("currentPage", clientPage.getNumber());
            response.put("totalItems", clientPage.getTotalElements());
            response.put("totalPages", clientPage.getTotalPages());
            return new ResponseEntity<>(response, HttpStatus.OK);

        } catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public ClientDTO updateClient(ClientDTO client) {

        clientService.getClientById(client.getId());  //getClientById verifie l'existence du client via la fonction requireOne()

        return clientService.save(clientMapper.DTOtoEntity(client));
    }

    @Override
    public void deleteClient(Long id) {

        clientService.deleteClient(id);
    }

    @Override
    public ResponseEntity<Map<String, Double>> getSalaireMoyenParProfession(){

        Map<String, Double> result = clientService.calculerSalaireMoyenParProfession();
        return ResponseEntity.ok(result);

    }
}
