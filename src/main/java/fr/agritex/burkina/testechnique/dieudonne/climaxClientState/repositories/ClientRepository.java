package fr.agritex.burkina.testechnique.dieudonne.climaxClientState.repositories;

import fr.agritex.burkina.testechnique.dieudonne.climaxClientState.entities.Client;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ClientRepository extends JpaRepository<Client,Long> {
    @Query(value = "select * from client s where CONCAT( LOWER(s.nom), ' ',LOWER(s.prenom)) like %:keyword%",nativeQuery = true)
    Page<Client> findByKeyboard(@Param("keyword") String keyword, Pageable pageable);

    List<Client> getByProfession(String profession);
}
