package fr.agritex.burkina.testechnique.dieudonne.climaxClientState.handler;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class ErrorDTO {

    private Integer httpCode;
    private String message;
    private List<String> erroList = new ArrayList<>();
}

