package fr.agritex.burkina.testechnique.dieudonne.climaxClientState.utils;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Optional;

@Component
public class FileUtils {
    private static String uploadDir;

    @Value("${file.upload-dir}")
    public void setUploadDir(String uploadDir) {
        FileUtils.uploadDir = uploadDir;
    }
    public static String getFileExtension(String filePath) {
        return Optional.ofNullable(filePath)
                .filter(f -> f.contains("."))
                .map(f -> f.substring(filePath.lastIndexOf(".") + 1))
                .orElse("");
    }
    public static Path saveFile(MultipartFile file) throws IOException {

        Path tempDirectory = Paths.get(uploadDir).toAbsolutePath().normalize();
        Files.createDirectories(tempDirectory); // Crée le répertoire si nécessaire

        String filename = file.getOriginalFilename();
        Path tempFile = tempDirectory.resolve(filename);
        Files.copy(file.getInputStream(), tempFile, StandardCopyOption.REPLACE_EXISTING);
        return tempFile;
    }
}
