package fr.agritex.burkina.testechnique.dieudonne.climaxClientState.services;


import fr.agritex.burkina.testechnique.dieudonne.climaxClientState.dto.ClientDTO;
import fr.agritex.burkina.testechnique.dieudonne.climaxClientState.mappers.ClientMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;


@Service
public class TextFileProcessor implements FileProcessorService {

    @Autowired
    private ClientService clientService;
    @Autowired
    private ClientMapper clientMapper;

    @Override
    public String getSupportedFileType() {
        return "txt";
    }

    @Override
    public void process(String filePath) {
        try (Stream<String> stream = Files.lines(Paths.get(filePath))) {
            stream.forEach(line -> {
                String[] parts = line.split(" ");
                if (parts.length < 3) {
                    return ;
                }

                ClientDTO clientDTO = new ClientDTO();
                clientDTO.setNom(parts[0]);
                clientDTO.setPrenom(parts[1]);
                clientDTO.setAge(Integer.parseInt(parts[2]));
                clientDTO.setProfession(parts[3]);
                clientDTO.setSalaire(Integer.parseInt(parts[4]));

                clientService.save(clientMapper.DTOtoEntity(clientDTO));
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
