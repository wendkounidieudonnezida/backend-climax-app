package fr.agritex.burkina.testechnique.dieudonne.climaxClientState.api;


import com.sun.istack.NotNull;
import fr.agritex.burkina.testechnique.dieudonne.climaxClientState.dto.ClientDTO;
import io.swagger.annotations.Api;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

import static fr.agritex.burkina.testechnique.dieudonne.climaxClientState.utils.Constant.APP_ROOT;


@Api(APP_ROOT+"/client")
@RequestMapping(APP_ROOT+"/client")
public interface ClientRestController {

    @PostMapping(path = "/save")
    ClientDTO save(@NotNull @RequestBody  ClientDTO client);

    @GetMapping(path = "/{id}")
    ClientDTO getClientById( @NotNull @PathVariable("id") Long id);

    @GetMapping(path ="/all")
    ResponseEntity<Map<String, Object>> getAllAndfindByKeyboard(@RequestParam(defaultValue = "0") int page
            , @RequestParam(defaultValue = "3") int size, @RequestParam(required = false) String search);

    @PutMapping(path = "/update")
    ClientDTO updateClient(@NotNull @RequestBody ClientDTO client);

    @DeleteMapping("/{id}")
    void deleteClient( @NotNull @PathVariable("id") Long id);

    @GetMapping("/salaire-moyen")
    ResponseEntity<Map<String, Double>> getSalaireMoyenParProfession();

}
